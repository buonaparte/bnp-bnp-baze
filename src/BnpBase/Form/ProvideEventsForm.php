<?php

namespace BnpBase\Form;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Form\Form;
use Zend\Form\FormInterface;

class ProvideEventsForm extends Form implements
    EventManagerAwareInterface
{
    private $events;
    private $pendingEvent;

    public function init()
    {
        parent::init();

        $this->getEventManager()->attachAggregate(new FormListenerAggregate());
    }

    public function setData($data)
    {
        $events = $this->getEventManager();

        $event = $this->getEvent();
        $event->setName(FormEvent::EVENT_SET_DATA);
        $event->setForm($this);
        $event->setData($data);

        $events->trigger($event);
        $result = parent::setData($event->getData());

        $event->setName(FormEvent::EVENT_POST_SET_DATA);

        $events->trigger($event);
        return $result;
    }

    public function bind($object, $flags = FormInterface::VALUES_NORMALIZED)
    {
        $events = $this->getEventManager();

        $event = $this->getEvent();
        $event->setForm($this);
        $event->setName(FormEvent::EVENT_BIND);
        $event->setObject($object);

        $events->trigger($event);
        $result = parent::bind($event->getObject(), $flags);

        $event->setName(FormEvent::EVENT_POST_BIND);

        $events->trigger($event);
        return $result;
    }

    /**
     * @return FormEvent
     */
    protected function getEvent()
    {
        if (null === $this->pendingEvent) {
            $this->pendingEvent = new FormEvent();
        }

        return $this->pendingEvent;
    }

    /**
     * Inject an EventManager instance
     *
     * @param  EventManagerInterface $eventManager
     * @return void
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->events = $eventManager;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }
}