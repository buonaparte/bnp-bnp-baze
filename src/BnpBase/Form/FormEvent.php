<?php

namespace BnpBase\Form;

use BnpBase\Form\Exception\InvalidArgumentException;
use Zend\EventManager\Event;
use Zend\Form\FormInterface;
use Zend\Stdlib\Parameters;

class FormEvent extends Event
{
    const EVENT_BIND = 'form.bind';

    const EVENT_POST_BIND = 'form.bind.post';

    const EVENT_SET_DATA = 'form.setData';

    const EVENT_POST_SET_DATA = 'form.setData.post';

    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var Parameters
     */
    protected $data;

    /**
     * @var object
     */
    protected $object;

    /**
     * @param FormInterface $form
     * @return FormEvent
     */
    public function setForm(FormInterface $form)
    {
        $this->form = $form;
        return $this;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param mixed $data
     * @return FormEvent
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param object $object
     * @return FormEvent
     * @throws Exception\InvalidArgumentException
     */
    public function setObject($object)
    {
        if (! is_object($object)) {
            throw new InvalidArgumentException(sprintf('Object expected, received %s', gettype($object)));
        }

        $this->object = $object;
        return $this;
    }

    /**
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @return Parameters
     */
    public function getData()
    {
        return $this->data;
    }
}