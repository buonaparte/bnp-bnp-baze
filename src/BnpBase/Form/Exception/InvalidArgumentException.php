<?php

namespace BnpBase\Form\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}