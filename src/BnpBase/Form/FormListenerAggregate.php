<?php

namespace BnpBase\Form;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Form\Element\File;
use Zend\Form\ElementInterface;
use Zend\Form\FieldsetInterface;
use Zend\Http\Request;
use Zend\Stdlib\CallbackHandler;
use Zend\Stdlib\Parameters;

class FormListenerAggregate implements ListenerAggregateInterface
{
    protected $listeners = array();

    protected function listen(EventManagerInterface $events, $name, $listener)
    {
        if (! array_key_exists($name, $this->listeners)) {
            $this->listeners[$name] = array();
        }

        $this->listeners[$name][] = new CallbackHandler($listener);
        $events->attach($name, $listener);
    }

    public function populateFromRequest(FormEvent $event)
    {
        $request = $event->getData();
        if (! $request instanceof Request) {
            return;
        }

        $form = $event->getForm();
        if (! $form->hasAttribute('method')) {
            $form->setAttribute('method', 'get');
        }

        switch (strtolower($form->getAttribute('method'))) {
            case 'get':
                $event->setData($request->getQuery());
                break;

            case 'post':
                $data = $request->getPost();
                if (0 !== $request->getFiles()->count()) {
                    $data = array_merge_recursive($data->toArray(), $request->getFiles()->toArray());
                }
                $event->setData($data);
        }
    }

    public function prePopulateData(FormEvent $event)
    {
        $data = $event->getData();
        $this->doPrePopulate($data, $event->getForm());
    }

    protected function doPrePopulate(&$data, $element)
    {
        if (! $element instanceof FieldsetInterface) {
            return;
        }

        if (empty($data)) {
            $data = new Parameters();
        }

        foreach ($element as $child) {
            // Skip File Elements (AbstractActionController::prgfile() Helper)
            if ($child instanceof File) {
                continue;
            }

            /** @var $child ElementInterface */
            if (! isset($data[$child->getName()])) {
                $data[$child->getName()] = null;
            }

            $section = $data[$child->getName()];
            $this->doPrePopulate($section, $child);
            $data[$child->getName()] = $section;
        }
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listen($events, FormEvent::EVENT_SET_DATA, array($this, 'populateFromRequest'));
        $this->listen($events, FormEvent::EVENT_SET_DATA, array($this, 'prePopulateData'));
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $listeners) {
            array_map(function ($listener) use ($events) {
                $events->detach($listener);
            }, $listeners);
        }

        $this->listeners = array();
    }
}