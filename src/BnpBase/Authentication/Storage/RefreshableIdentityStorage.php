<?php

namespace BnpBase\Authentication\Storage;

use BnpBase\Authentication\Adapter\MapperAdapter;
use BnpBase\Mapper\FilterInterface;
use Zend\Authentication\Exception\UnexpectedValueException;
use Zend\Authentication\Storage\StorageInterface;
use Zend\EventManager\Event;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;

class RefreshableIdentityStorage implements
    StorageInterface,
    EventManagerAwareInterface
{
    const EVENT_POST_READ = 'storage.read.post';

    const EVENT_WRITE = 'storage.write';

    /**
     * @var MapperAdapter
     */
    protected $mapperAdapter;

    /**
     * @var StorageInterface
     */
    protected $delegate;

    protected $identity;

    protected $events;

    public function __construct(StorageInterface $delegate, MapperAdapter $mapperAdapter)
    {
        $this->delegate = $delegate;
        $this->mapperAdapter = $mapperAdapter;
    }

    /**
     * Returns true if and only if storage is empty
     *
     * @throws \Zend\Authentication\Exception\ExceptionInterface If it is impossible to determine whether storage is empty
     * @return bool
     */
    public function isEmpty()
    {
        return null === $this->identity && $this->delegate->isEmpty();
    }

    /**
     * Returns the contents of storage
     *
     * Behavior is undefined when storage is empty.
     *
     * @throws \Zend\Authentication\Exception\ExceptionInterface If reading contents from storage is impossible
     * @return mixed
     */
    public function read()
    {
        if (null === $this->identity) {
            if (null === $res = $this->delegate->read()) {
                return null;
            }

            $mapper = $this->mapperAdapter->getMapper();
            /** @var $filter FilterInterface */
            $filter = $mapper->getFilter();

            try {
                $identity = $mapper->findOne($filter->add($this->mapperAdapter->getIdentityField(), $res));
                if (! $identity) {
                    throw new \Exception();
                }
            } catch (\Exception $e) {
                $this->delegate->clear();
                return null;
            }

            $event = new Event(static::EVENT_POST_READ, $this);
            $event->setParam('identity', $identity);

            $this->getEventManager()->trigger($event);

            $this->identity = $event->getParam('identity', $identity);
        }

        return $this->identity;
    }

    /**
     * Writes $contents to storage
     *
     * @param  mixed $contents
     * @throws \Zend\Authentication\Exception\ExceptionInterface If writing $contents to storage is impossible
     * @return void
     */
    public function write($contents)
    {
        $event = new Event(static::EVENT_WRITE, $this);
        $event->setParam('contents', $contents);
        $this->getEventManager()->trigger($event);

        if ($event->propagationIsStopped()) {
            return;
        }

        $identityClass = $this->mapperAdapter->getIdentityClass();

        $contents = $event->getParam('contents', $contents);
        if (! is_object($contents) || ! $contents instanceof $identityClass) {
            throw new UnexpectedValueException();
        }

        $data = $this->mapperAdapter->getIdentityHydrator()->extract($contents);
        if (empty($data[$this->mapperAdapter->getIdentityField()])) {
            throw new UnexpectedValueException();
        }

        $this->delegate->write($data[$this->mapperAdapter->getIdentityField()]);
    }

    /**
     * Clears contents from storage
     *
     * @throws \Zend\Authentication\Exception\ExceptionInterface If clearing contents from storage is impossible
     * @return void
     */
    public function clear()
    {
        $this->identity = null;
        $this->delegate->clear();
    }

    /**
     * Inject an EventManager instance
     *
     * @param  EventManagerInterface $eventManager
     * @return void
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->events = $eventManager;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }
}