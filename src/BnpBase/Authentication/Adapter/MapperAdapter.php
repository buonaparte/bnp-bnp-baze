<?php

namespace BnpBase\Authentication\Adapter;

use BnpBase\Mapper\BaseMapperInterface;
use BnpBase\Mapper\FilterInterface;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result as AuthResult;
use Zend\EventManager\Event;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Stdlib\Hydrator\HydratorInterface;

class MapperAdapter implements
    AdapterInterface,
    EventManagerAwareInterface
{
    const EVENT_AUTHENTICATE = 'adapter.authenticate';

    const RESULT_CONTAINER_KEY_CODE = 'code';
    const RESULT_CONTAINER_KEY_IDENTITY = 'identity';
    const RESULT_CONTAINER_KEY_MESSAGES = 'messages';

    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var BaseMapperInterface
     */
    protected $mapper;

    /**
     * @var string
     */
    protected $identityField;

    /**
     * @var string
     */
    protected $credentialField;

    /**
     * @var string
     */
    protected $identityValue;

    /**
     * @var string
     */
    protected $credentialValue;

    /**
     * @var callable
     */
    protected $credentialChecker;

    /**
     * @var array
     */
    protected $authenticationResultContainer;

    /**
     * @var string
     */
    protected $identityClass;

    /**
     * @var HydratorInterface
     */
    protected $identityHydrator;

    /**
     * @var AuthResult
     */
    protected $authenticationResult;

    /**
     * @param BaseMapperInterface $mapper
     */
    public function __construct(BaseMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    public function getMapper()
    {
        return $this->mapper;
    }

    /**
     * @param string $field
     * @return MapperAdapter
     */
    public function setIdentityField($field)
    {
        $this->identityField = $field;
        return $this;
    }

    /**
     * @param string $field
     * @return MapperAdapter
     */
    public function setCredentialField($field)
    {
        $this->credentialField = $field;
        return $this;
    }

    /**
     * @param string $value
     * @return MapperAdapter
     */
    public function setIdentity($value)
    {
        $this->identityValue = $value;
        return $this;
    }

    /**
     * @param string $value
     * @return MapperAdapter
     */
    public function setCredential($value)
    {
        $this->credentialValue = $value;
        return $this;
    }

    public function getIdentityField()
    {
        if (! $this->identityField) {
            throw new \RuntimeException('Identity field not defined');
        }

        return $this->identityField;
    }

    public function getCredentialField()
    {
        if (! $this->credentialField) {
            throw new \RuntimeException('Credential field was not defiend');
        }

        return $this->credentialField;
    }

    public function setCredentialChecker($checker)
    {
        if (! is_callable($checker)) {
            throw new \InvalidArgumentException(sprintf(
                'A credential checker must be a valid callable, %s received', gettype($checker)));
        }

        $this->credentialChecker = $checker;
        return $this;
    }

    protected function getCredentialChecker()
    {
        if (null === $this->credentialChecker) {
            $this->setCredentialChecker(function ($credentialValue, $storedValue) {
                return $credentialValue == $storedValue;
            });
        }

        return $this->credentialChecker;
    }

    public function setIdentityClass($class)
    {
        $this->identityClass = $class;
        return $this;
    }

    public function getIdentityClass()
    {
        if (null === $this->identityClass) {
            throw new \RuntimeException('Identity class not specified');
        }

        if (! class_exists($this->identityClass) && ! interface_exists($this->identityClass)) {
            throw new \InvalidArgumentException(sprintf('Identity class %s is not loaded', $this->identityClass));
        }

        return $this->identityClass;
    }

    public function setIdentityHydrator(HydratorInterface $hydrator)
    {
        $this->identityHydrator = $hydrator;
        return $this;
    }

    public function getIdentityHydrator()
    {
        if (null === $this->identityHydrator) {
            $this->setIdentityHydrator(new ClassMethods());
        }

        return $this->identityHydrator;
    }

    protected function init()
    {
        if (null === $this->identityValue) {
            throw new \RuntimeException('Identity Value was not specified prior Authentication');
        }

        if (null === $this->credentialValue) {
            throw new \RuntimeException('Credential Value was not specified prior Authentication');
        }

        if (null === $this->authenticationResultContainer) {
            $this->authenticationResultContainer = array();
        }

        $this->authenticationResultContainer = array(
            self::RESULT_CONTAINER_KEY_CODE => AuthResult::FAILURE,
            self::RESULT_CONTAINER_KEY_IDENTITY => null,
            self::RESULT_CONTAINER_KEY_MESSAGES => array()
        );
    }

    protected function createAuthenticationResult()
    {
        return new AuthResult(
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_CODE],
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_IDENTITY],
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_MESSAGES]
        );
    }

    /**
     * @param object $identity
     * @return AuthResult
     */
    protected function validateIdentity($identity)
    {
        $identityClass = $this->getIdentityClass();
        if (! $identity instanceof $identityClass) {
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_CODE] = AuthResult::FAILURE;
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_MESSAGES][] = sprintf(
                'Retrieved identity is not an Instance of %s', get_class($identity), $identityClass);

            return $this->createAuthenticationResult();
        }

        /** @var $identity object */
        $identityData = $this->getIdentityHydrator()->extract($identity);
        if (! array_key_exists($this->credentialField, $identityData)) {
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_CODE] = AuthResult::FAILURE;
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_MESSAGES][] = sprintf(
                'Specified credential field "%s" could not be extracted from the Identity',
                $this->credentialField
            );

            return $this->createAuthenticationResult();
        }

        $result = call_user_func(
            $this->getCredentialChecker(),
            $this->credentialValue,
            $identityData[$this->credentialField]
        );

        if (! $result) {
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_CODE] = AuthResult::FAILURE_CREDENTIAL_INVALID;
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_MESSAGES][] =
                'Invalid Crendential';
            return $this->createAuthenticationResult();
        }

        $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_CODE] = AuthResult::SUCCESS;
        $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_IDENTITY] = $identity;
        $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_MESSAGES][]
            = 'Authentication successful';

        return $this->createAuthenticationResult();
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
     */
    public function authenticate()
    {
        $this->init();

        /** @var $filter FilterInterface */
        $filter = $this->mapper->getFilter();
        $filter->add($this->identityField, $this->identityValue);

        $identity = $this->mapper->findOne($filter);
        if (! $identity) {
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_CODE] = AuthResult::FAILURE_IDENTITY_NOT_FOUND;
            $this->authenticationResultContainer[self::RESULT_CONTAINER_KEY_MESSAGES][] = 'Identity not found';

            $res = $this->createAuthenticationResult();
        } else {
            $res = $this->validateIdentity($identity);
        }

        $event = new Event(static::EVENT_AUTHENTICATE);
        $event->setParam('result', $res);

        $this->getEventManager()->trigger($event);

        return $event->getParam('result', $res);
    }

    /**
     * Inject an EventManager instance
     *
     * @param  EventManagerInterface $eventManager
     * @return void
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->events = $eventManager;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }
}