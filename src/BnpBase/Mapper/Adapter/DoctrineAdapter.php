<?php

namespace BnpBase\Mapper\Adapter;

use BnpBase\Mapper\BaseFilter;
use BnpBase\Mapper\Filter\DoctrineFilter;
use BnpBase\Mapper\FilterInterface;
use BnpBase\Mapper\MapperAdapterInterface;
use Doctrine\ORM\EntityManager;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator;
use Zend\Paginator\Paginator;

class DoctrineAdapter implements MapperAdapterInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $entityClass;

    public function __construct(EntityManager $entityManager, $entityClass)
    {
        $this->entityManager = $entityManager;
        $this->entityClass = $entityClass;
    }

    /**
     * @param FilterInterface $condition
     * @param bool $asPaginator
     * @return \Traversable
     */
    public function find(FilterInterface $condition = null, $asPaginator = true)
    {
        if (null === $condition) {
            $condition = $this->createFilter();
        }
        /** @var $condition BaseFilter */
        $condition = $condition->getBridge();

        $alias = null;
        /** @var $condition DoctrineFilter */
        $query = $condition->getQuery($alias)->getQuery();

        if ($asPaginator) {
            return new Paginator(new DoctrinePaginator(new \Doctrine\ORM\Tools\Pagination\Paginator($query)));
        }

        return $query->execute();
    }

    /**
     * @param FilterInterface $condition
     * @return object
     */
    public function findOne(FilterInterface $condition = null)
    {
        /** @var $condition BaseFilter */
        if (null === $condition) {
            $condition = $this->createFilter();
        }

        $condition = $condition->getBridge();

        $alias = null;
        /** @var $condition DoctrineFilter */
        $result = $condition->getQuery($alias)
                         ->getQuery()
                         ->setMaxResults(1)
                         ->execute();

        return empty($result[0]) ? null : $result[0];
    }

    /**
     * @param object $entity
     * @return void
     */
    public function create($entity)
    {
        $this->persist($entity);
    }

    /**
     * @param object $entity
     * @return void
     */
    public function update($entity)
    {
        $this->persist($entity);
    }

    /**
     * @param object $entity
     * @return void
     */
    public function remove($entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    protected function persist($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * @return FilterInterface
     */
    public function createFilter()
    {
        return new DoctrineFilter($this->entityManager->getRepository($this->entityClass));
    }
}