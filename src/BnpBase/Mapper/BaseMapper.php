<?php

namespace BnpBase\Mapper;

use Zend\EventManager\Event;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Stdlib\CallbackHandler;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Stdlib\Hydrator\HydratorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;

class BaseMapper implements
    BaseMapperInterface,
    EventManagerAwareInterface,
    HydratorAwareInterface,
    ListenerAggregateInterface
{
    const PRIORITY_LOW = -9999;
    const PRIORITY_HIGH = 9999;

    const EVENT_FIND = 'mapper.find';
    const EVENT_POST_FIND = 'mapper.find.post';

    const EVENT_UPDATE = 'mapper.update';
    const EVENT_POST_UPDATE = 'mapper.update.post';

    const EVENT_CREATE = 'mapper.create';
    const EVENT_POST_CREATE = 'mapper.create.post';

    const EVENT_REMOVE = 'mapper.remove';
    const EVENT_POST_REMOVE = 'mapper.remove.post';

    protected $adapter;
    protected $hydrator;
    protected $entityPrototype;
    protected $entitySpecifications;
    protected $pendingFilter;
    protected $events;
    private $listeners;

    public function __construct(MapperAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    public function findById($id)
    {
        return $this->findOne($this->filter(function (FilterInterface $filter) use ($id) {
            $filter->add('id', $id);
        }));
    }

    public function findOne(FilterInterface $filter = null)
    {
        if (null === $filter) {
            $filter = $this->getFilter();
        }

        $event = $this->createEvent(static::EVENT_FIND);
        $event->setParam('filter', $filter);

        $events = $this->getEventManager();

        $adapter = $this->adapter;
        $events->attach(static::EVENT_FIND, function (Event $event) use ($adapter, $filter) {
            $event->setParam('entity', $adapter->findOne($event->getParam('filter', $filter)));
        }, static::PRIORITY_LOW);

        $events->trigger($event);
        $events->trigger(static::EVENT_POST_FIND, $event);
        $this->pendingFilter = null;

        return $event->getParam('entity', null);
    }

    public function findAll(FilterInterface $filter = null, $asPaginator = true)
    {
        if (null === $filter) {
            $filter = $this->getFilter();
        }

        $event = $this->createEvent(static::EVENT_FIND);
        $event->setParam('filter', $filter);

        $this->getEventManager()->trigger($event);

        $res = $this->adapter->find($event->getParam('filter', $filter), $asPaginator);
        $this->pendingFilter = null;

        return $res;
    }

    public function update($entity)
    {
        $event = $this->createEvent(static::EVENT_UPDATE);
        $event->setParam('entity', $entity);

        $events = $this->getEventManager();

        $adapter = $this->adapter;
        $events->attach(static::EVENT_POST_UPDATE, function (Event $event) use ($adapter, $entity) {
            $adapter->update($event->getParam('entity', $entity));
        }, static::PRIORITY_HIGH);

        $events->trigger($event);
        if (! $event->propagationIsStopped()) {
            $events->trigger(static::EVENT_POST_UPDATE, $event);
        }
    }

    public function create($entity)
    {
        $event = $this->createEvent(static::EVENT_CREATE);
        $event->setParam('entity', $entity);

        $events = $this->getEventManager();

        $adapter = $this->adapter;
        $events->attach(static::EVENT_POST_CREATE, function (Event $event) use ($adapter, $entity) {
            $adapter->create($event->getParam('entity', $entity));
        }, static::PRIORITY_HIGH);

        $events->trigger($event);
        if (! $event->propagationIsStopped()) {
            $events->trigger(static::EVENT_POST_CREATE, $event);
        }
    }

    public function remove($entity)
    {
        $event = $this->createEvent(static::EVENT_REMOVE);
        $event->setParam('entity', $entity);

        $events = $this->getEventManager();

        $adapter = $this->adapter;
        $events->attach(static::EVENT_POST_REMOVE, function (Event $event) use ($adapter, $entity) {
            $adapter->remove($event->getParam('entity', $entity));
        }, static::PRIORITY_HIGH);

        $events->trigger($event);
        if (! $event->propagationIsStopped()) {
            $events->trigger(static::EVENT_POST_REMOVE, $event);
        }
    }

    protected function createEvent($eventName)
    {
        return new Event($eventName, $this);
    }

    /**
     * @return FilterInterface
     */
    public function getFilter()
    {
        if (null === $this->pendingFilter) {
            $fiterBridge = $this->adapter->createFilter();
            $this->pendingFilter = new BaseFilter($fiterBridge);
            $this->pendingFilter->getEventManager()->attachAggregate($this);

            if ($fiterBridge instanceof ListenerAggregateInterface) {
                $this->pendingFilter->getEventManager()->attachAggregate($fiterBridge);
            }
        }

        return $this->pendingFilter;
    }

    public function filter($callback)
    {
        if (! is_callable($callback)) {
            throw new \InvalidArgumentException('A valid callable must be provided as a filter');
        }

        $filter = $this->getFilter();
        call_user_func($callback, $filter);

        return $filter;
    }

    /**
     * Inject an EventManager instance
     *
     * @param  EventManagerInterface $eventManager
     * @return void
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->events = $eventManager;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

    /**
     * Set hydrator
     *
     * @param  HydratorInterface $hydrator
     * @return HydratorAwareInterface
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
        return $this;
    }

    /**
     * Retrieve hydrator
     *
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        if (null === $this->hydrator) {
            $this->hydrator = new ClassMethods();
        }

        return $this->hydrator;
    }

    public function setEntityPrototype($object)
    {
        $this->entityPrototype = $object;
        return $this;
    }

    protected function getEntityPrototype()
    {
        if (null === $this->entityPrototype) {
            throw new \LogicException('Entity Prototype was not set');
        }

        return $this->entityPrototype;
    }

    protected function getEntitySpecifications()
    {
        if (null === $this->entitySpecifications) {
            $this->entitySpecifications = array_keys($this->getHydrator()->extract($this->getEntityPrototype()));
        }

        return $this->entitySpecifications;
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listen($events, '*', array($this, 'onFilter'));
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $listener) {
            $events->detach($listener);
        }

        $this->listeners = array();
    }

    protected function listen(EventManagerInterface $events, $event, $listener)
    {
        if (! $listener instanceof CallbackHandler) {
            $listener = new CallbackHandler($listener);
        }

        $this->listeners[] = $listener;
        $events->attach($event, $listener);

        return $this;
    }

    public function onFilter(EventInterface $event)
    {
        $field = null;
        if (BaseFilter::EVENT_CONDITION === $event->getName()) {
            $exprRight = $event->getParam('expr_right');
            if (is_array($exprRight) || $exprRight instanceof \Traversable) {
                foreach ($exprRight as $field => $bindValue) {
                    $this->checkFieldExistence($field);
                }
            } elseif (null !== $exprRight) {
                $field = $event->getParam('expr_left');
            }
        } elseif (BaseFilter::EVENT_ORDER === $event->getName()) {
            $field = $event->getParam('field');
        }

        if (null !== $field) {
            $this->checkFieldExistence($field);
        }
    }

    protected function checkFieldExistence($field)
    {
        if (! in_array($field, $this->getEntitySpecifications())) {
            throw new \InvalidArgumentException(sprintf('Field %s is not reachable for the %e Entity using %s Hydrator',
                $field, get_class($this->getEntityPrototype()), get_class($this->getHydrator())));
        }
    }
}