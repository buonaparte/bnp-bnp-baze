<?php

namespace BnpBase\Mapper;

interface BaseMapperInterface
{
    public function findById($id);

    public function findOne(FilterInterface $filter = null);

    public function findAll(FilterInterface $filter = null, $asPaginator = true);

    public function update($entity);

    public function create($entity);

    public function remove($entity);

    public function getFilter();
}