<?php

namespace BnpBase\Mapper\Filter;

use BnpBase\Mapper\FilterInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class DoctrineFilter implements FilterInterface
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $entityRepository;

    /**
     * @var array
     * @internal \Doctrine\ORM\QueryBuilder
     */
    protected $pendingQueryBuilders = array();

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @param $alias
     * @return QueryBuilder
     */
    public function getQuery(&$alias)
    {
        if (null === $alias) {
            $alias = $this->generateAliasName();
        }

        if (! array_key_exists($alias, $this->pendingQueryBuilders)) {
            $this->pendingQueryBuilders[$alias] = $this->entityRepository->createQueryBuilder($alias);
        }

        return $this->pendingQueryBuilders[$alias];
    }

    /**
     * @return string
     */
    protected function generateAliasName()
    {
        $entityName = $this->entityRepository->getClassName();
        $entityNameParts = explode('\\', $entityName);

        return strtolower(substr(end($entityNameParts), 0, 1));
    }

    /**
     * @param string $field
     * @param string $order
     * @return FilterInterface
     */
    public function order($field, $order = self::ORDER_ASC)
    {
        $alias = null;
        $this->getQuery($alias)
             ->orderBy("$alias.$field", strtoupper($order));

        return $this;
    }

    /**
     * @param int $offset
     * @return FilterInterface
     */
    public function offset($offset)
    {
        $alias = null;
        $this->getQuery($alias)
             ->setFirstResult($offset);

        return $this;
    }

    /**
     * @param int $limit
     * @return FilterInterface
     */
    public function limit($limit)
    {
        $alias = null;
        $this->getQuery($alias)
             ->setMaxResults($limit);
    }

    /**
     * @param mixed $exprLeft
     * @param mixed|null $exprRight
     * @return FilterInterface
     */
    public function add($exprLeft, $exprRight = null)
    {
        $alias = null;
        $query = $this->getQuery($alias);

        if (is_array($exprRight) || $exprRight instanceof \Traversable) {
            $query->add('where', $exprLeft);
            foreach ($exprRight as $k => $v) {
                $query->setParameter($k, $v);
            }
        } elseif (null === $exprRight) {
            $query->add($exprLeft);
        } else {
            $query->andWhere("$alias.$exprLeft = :$exprLeft")
                  ->setParameter($exprLeft, $exprRight);
        }

        return $this;
    }
}