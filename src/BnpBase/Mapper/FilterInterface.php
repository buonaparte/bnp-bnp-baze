<?php

namespace BnpBase\Mapper;

interface FilterInterface
{
    const ORDER_ASC = 'asc';
    const ORDER_DESC = 'desc';

    /**
     * @param string $field
     * @param string $order
     * @return FilterInterface
     */
    public function order($field, $order = self::ORDER_ASC);

    /**
     * @param int $offset
     * @return FilterInterface
     */
    public function offset($offset);

    /**
     * @param int $limit
     * @return FilterInterface
     */
    public function limit($limit);

    /**
     * @param mixed $exprLeft
     * @param mixed|null $exprRight
     * @return FilterInterface
     */
    public function add($exprLeft, $exprRight = null);
}