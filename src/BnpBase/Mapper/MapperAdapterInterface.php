<?php

namespace BnpBase\Mapper;

interface MapperAdapterInterface
{
    /**
     * @param FilterInterface $condition
     * @param bool $asPaginator
     * @return \Traversable
     */
    public function find(FilterInterface $condition = null, $asPaginator = true);

    /**
     * @param FilterInterface $condition
     * @return object
     */
    public function findOne(FilterInterface $condition = null);

    /**
     * @param object $entity
     * @return void
     */
    public function create($entity);

    /**
     * @param object $entity
     * @return void
     */
    public function update($entity);

    /**
     * @param object $entity
     * @return void
     */
    public function remove($entity);

    /**
     * @return FilterInterface
     */
    public function createFilter();
}