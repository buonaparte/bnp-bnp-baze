<?php

namespace BnpBase\Mapper;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;

class BaseFilter implements
    FilterInterface,
    EventManagerAwareInterface
{
    const EVENT_CONDITION = 'mapper_filter.condition';
    const EVENT_ORDER = 'mapper_filter.order';
    const EVENT_LIMIT = 'mapper_filter.limit';
    const EVENT_OFFSET = 'mapper_filter.offset';

    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var FilterInterface
     */
    protected $bridge;

    public function __construct(FilterInterface $bridge)
    {
        $this->bridge = $bridge;
    }

    /**
     * Inject an EventManager instance
     *
     * @param  EventManagerInterface $eventManager
     * @return void
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->events = $eventManager;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

    /**
     * @param string $field
     * @param string $order
     * @return FilterInterface
     */
    public function order($field, $order = self::ORDER_ASC)
    {
        $this->getEventManager()->trigger(self::EVENT_ORDER, $this, array('field' => $field, 'order' => $order));
        $this->bridge->order($field, $order);

        return $this;
    }

    /**
     * @param int $offset
     * @return FilterInterface
     */
    public function offset($offset)
    {
        $this->getEventManager()->trigger(self::EVENT_OFFSET, $this, array('offset' => $offset));
        $this->bridge->offset($offset);

        return $this;
    }

    /**
     * @param int $limit
     * @return FilterInterface
     */
    public function limit($limit)
    {
        $this->getEventManager()->trigger(self::EVENT_LIMIT, $this, array('limit' => $limit));
        $this->bridge->limit($limit);

        return $this;
    }

    /**
     * @param mixed $exprLeft
     * @param mixed|null $exprRight
     * @return FilterInterface
     */
    public function add($exprLeft, $exprRight = null)
    {
        $args = array('expr_left' => $exprLeft, 'expr_right' => $exprRight);
        $this->getEventManager()->trigger(self::EVENT_CONDITION, $this, $args);
        $this->bridge->add($exprLeft, $exprRight);

        return $this;
    }

    /**
     * @return FilterInterface
     */
    public function getBridge()
    {
        return $this->bridge;
    }
}