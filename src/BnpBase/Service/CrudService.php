<?php

namespace BnpBase\Service;

use BnpBase\Mapper\BaseMapperInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Form\FormInterface;

class CrudService implements
    EventManagerAwareInterface
{
    const EVENT_CREATE = 'crud_service.create';
    const EVENT_POST_CREATE = 'crud_service.create.post';
    const EVENT_UPDATE = 'crud_service.update';
    const EVENT_POST_UPDATE = 'crud_service.update.post';

    /**
     * @var BaseMapperInterface
     */
    protected $mapper;

    /**
     * @var EventManagerInterface
     */
    protected $events;

    public function __construct(BaseMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param FormInterface $form
     * @param $data
     *
     * @return object
     */
    public function create(FormInterface $form, $data)
    {
        $entity = $form->getData();
        $args = array('form' => $form, 'entity' => $entity, 'data' => $data);

        $this->getEventManager()->trigger(static::EVENT_CREATE, $this, $args);
        $this->mapper->create($entity);
        $this->getEventManager()->trigger(static::EVENT_POST_CREATE, $this, $args);

        return $entity;
    }

    public function update(FormInterface $form, $data, $entity)
    {
        $args = array('form' => $form, 'entity' => $entity, 'data' => $data);

        $this->getEventManager()->trigger(static::EVENT_UPDATE, $this, $args);
        $this->mapper->update($entity);
        $this->getEventManager()->trigger(static::EVENT_POST_UPDATE, $this, $args);
    }

    /**
     * Inject an EventManager instance
     *
     * @param  EventManagerInterface $eventManager
     * @return void
     */
    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->events = $eventManager;
    }

    /**
     * Retrieve the event manager
     *
     * Lazy-loads an EventManager instance if none registered.
     *
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }
}