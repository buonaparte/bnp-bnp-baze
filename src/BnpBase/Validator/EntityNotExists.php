<?php

namespace BnpBase\Validator;

use Zend\Validator\Exception;

class EntityNotExists extends EntityExists
{
    const ERROR_ENTITY_FOUND = 'entityFound';

    protected $messageTemplates = array(
        self::ERROR_ENTITY_FOUND => 'An entity was found matching %value%'
    );

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
       if (parent::isValid($value)) {
            $this->error(self::ERROR_ENTITY_FOUND, $value);
            return false;
        }

        return true;
    }
}