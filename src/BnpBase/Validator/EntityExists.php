<?php

namespace BnpBase\Validator;

use BnpBase\Mapper\BaseMapper;
use BnpBase\Mapper\BaseMapperInterface;
use BnpBase\Mapper\FilterInterface;
use BnpBase\Validator\Exception\InvalidArgumentException;
use Zend\Stdlib\ArrayUtils;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class EntityExists extends AbstractValidator
{
    const ERROR_NO_ENTITY_FOUND = 'noEntityFound';

    const OPTIONS_KEY_MAPPER = 'mapper';
    const OPTIONS_KEY_FIELDS = 'fields';

    /**
     * @var array
     */
    protected $fields;

    /**
     * @var \BnpBase\Mapper\BaseMapper
     */
    protected $mapper;

    protected $messageTemplates = array(
        self::ERROR_NO_ENTITY_FOUND => 'No entity matching %value% found'
    );

    public function __construct(array $options)
    {
        parent::__construct($options);

        if (! isset($options[self::OPTIONS_KEY_MAPPER])) {
            throw new InvalidArgumentException('A mapper must be provided as an option');
        }

        if (! isset($options[self::OPTIONS_KEY_FIELDS])) {
            throw new InvalidArgumentException('No fields provided to validate');
        }

        $mapper = $options[self::OPTIONS_KEY_MAPPER];
        if (! $mapper instanceof BaseMapperInterface) {
            throw new InvalidArgumentException('A BaseMapperInterface instance expected');
        }

        $this->mapper = $mapper;
        $this->setFields($options[self::OPTIONS_KEY_FIELDS]);
    }

    protected function setFields($fields)
    {
        $this->fields = array();

        if (null === $fields) {
            $fields = array();
        }

        if (! is_array($fields) && ! $fields instanceof \Traversable) {
            throw new InvalidArgumentException(sprintf(
                'Expected array | Traversable as fields, %s provided', gettype($fields)));
        }

        foreach ($fields as $k => $v) {
            $this->fields[$k] = $v;
        }
    }

    protected function getFieldsToMatch($value)
    {
        $value = (array) $value;
        $matchedFields = array();

        if (ArrayUtils::isHashTable($value)) {
            $value = ArrayUtils::iteratorToArray($value);

            foreach ($this->fields as $k => $v) {
                if (! array_key_exists($k, $value)) {
                    throw new Exception\RuntimeException(sprintf(
                        'Field %s given to be validated was not found in the provided keys',
                        $k
                    ));
                }

                $matchedFields[$k] = $v;
            }
        } else {
            $matchedFields = @array_combine($this->fields, $value);

            if (! $matchedFields) {
                throw new Exception\RuntimeException('Number of fields do not correspond');
            }
        }

        return $matchedFields;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        $value = $this->getFieldsToMatch($value);

        $entity = $this->mapper->findOne($this->mapper->filter(function (FilterInterface $filter) use ($value) {
            foreach ($value as $k => $v) {
                $filter->add($k, $v);
            }
        }));

        if (! $entity) {
            $this->error(self::ERROR_NO_ENTITY_FOUND, $value);
            return false;
        }

        return true;
    }
}