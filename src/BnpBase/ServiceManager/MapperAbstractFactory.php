<?php

namespace BnpBase\ServiceManager;

use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MapperAbstractFactory implements AbstractFactoryInterface
{
    /**
     * Mappers Namespaces (wildcards accepted)
     *
     * @var string
     */
    protected $mapperNamespaces = array('*Mapper');

    /**
     * Mappers Suffixes (Namespaces fallback)
     *
     * @var string
     */
    protected $mapperSuffixes = array('Mapper');

    /**
     * Entity Namespaces (wildcards accepted)
     *
     * @var string
     */
    protected $entityNamespaces = array('*Entity');

    /**
     * Determine if we can create a service with name
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @param $name
     * @param $requestedName
     * @return bool
     */
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {

    }

    protected function isMapperRequested($requestedName, array &$matches)
    {
        foreach ($this->mapperNamespaces as $namespace) {
            if (preg_match("#({$this->wildcardToRegex($namespace)})#", $requestedName, $parts)) {
                $namespace = $parts[1];
                $name = trim(str_replace($namespace, '', $requestedName), '\\');

            }
        }
    }

    protected function wildcardToRegex($target)
    {
        return str_replace(
            array('%', '*'),
            array('(.{1})', '(.*)'),
            $target
        );
    }

    /**
     * Create service with name
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @param $name
     * @param $requestedName
     * @return mixed
     */
    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        // TODO: Implement createServiceWithName() method.
    }
}